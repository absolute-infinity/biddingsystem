package org.bidding

import akka.actor.ActorRef
import akka.http.scaladsl.model.{HttpEntity, HttpMethods, HttpRequest, MediaTypes}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{Matchers, WordSpec}
import akka.http.scaladsl.model.StatusCodes
import spray.json.JsValue

class HttpBidTest extends WordSpec with Matchers with ScalatestRouteTest with AuctionRoutes {
  "Bidding API" should {
    "Posting to /bid should check for valid bid" in {

      val jsonRequest =
        """
          |{
          |  "id": "Testbid1",
          |  "imp": [
          |    {
          |      "id": "121-dt1",
          |      "wmin": 1,
          |      "wmax": 2,
          |      "w": 1,
          |      "hmin": 1,
          |      "hmax": 2,
          |      "h": 1,
          |      "bidFloor": 1000
          |    },
          |    {
          |      "id": "121-dt23",
          |      "wmin": 1,
          |      "wmax": 3,
          |      "w": 1,
          |      "hmin": 1,
          |      "hmax": 4,
          |      "h": 1,
          |      "bidFloor": 1000
          |    }
          |  ],
          |  "site": {
          |    "id": 12345,
          |    "domain": "asd"
          |  },
          |  "user": {
          |    "id": "q1",
          |    "geo": {
          |      "country": "Bangladesh",
          |      "city": "Dhaka",
          |      "lat": 12336522,
          |      "lon": 8541
          |    }
          |  },
          |  "device": {
          |    "id": "q1",
          |    "geo": {
          |      "country": "Bangladesh",
          |      "city": "Dhaka",
          |      "lat": 12336522,
          |      "lon": 8541
          |    }
          |  }
          |}
        """.stripMargin



      val postRequest = HttpRequest(
        HttpMethods.POST,
        uri = "/bid",
        entity = HttpEntity(MediaTypes.`application/json`, jsonRequest))




      postRequest ~> auctionRoutes ~> check {
        status shouldEqual StatusCodes.OK

      }
    }

  }

  // other dependencies that UserRoutes use
  override def biddingAgentActor: ActorRef = system.actorOf(BiddingAgent.props("biddingAgent"), "biddingAgent")
}