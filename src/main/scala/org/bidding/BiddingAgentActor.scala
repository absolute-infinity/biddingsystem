package org.bidding

import akka.actor.{Actor, ActorLogging, Props}
import spray.json.DefaultJsonProtocol._
import spray.json._

object BiddingAgent {
  def props(name: String): Props = Props(new BiddingAgent(name))

}

class BiddingAgent(name: String) extends Actor with ActorLogging with Domain {

  override def preStart(): Unit = log.info("BiddingAgent started")

  override def postStop(): Unit = log.info("BiddingAgent stopped")

  // request dispatch and find a valid bid for predefined campaign list
  def dispatchBidRequest(bidRequest: BidRequest): Option[String] = {
    implicit val bannerFormat = jsonFormat4(Banner)
    implicit val bidResponse = jsonFormat5(BidResponse)

    if (bidRequest.imp.isEmpty) {
      return None
    } // check for atleast one impression exists
    else {
      for (impList <- bidRequest.imp) {
        for (impression <- impList) {
          println(impression.wmax)
          val widthList = impression.wmax :: impression.wmin :: impression.w :: Nil
          val heightList = impression.hmax :: impression.hmin :: impression.h :: Nil

          if (widthList == Nil && heightList == Nil) return None // check for wmax|wmin|w & hmax|hmin|h existance and make list for future development, if needed
          else {
            println("width " + widthList.head + " height " + heightList.head)

            val Some(height) = heightList.head match {
              case Some(hei) => heightList.head
            }
            val Some(width) = widthList.head match {
              case Some(wid) => widthList.head
            }
            val Some(bid) = impression.bidFloor match {
              case Some(bidPrice) => impression.bidFloor
              case None => None
            }
            println("bid price  " + bid + " width " + width + " height " + height)

            if (bid != None) {
              for (campaign <- campaignList) {
                if (bid <= campaign.bid) {
                  // check bid is less or equal to existing list
                  for (banner <- campaign.banners) {
                    if (width == banner.width && height == banner.height) {
                      return Some(BidResponse("RSPS", bidRequest.id, bid, None, Some(banner)).toJson.toString())
                    }
                  }
                }
              }
            }

          }
        }
      }

    }

    return None

  }

  override def receive: Receive = {
    case Bid(bidRequest) =>
      log.info(bidRequest.id)
      sender() ! dispatchBidRequest(bidRequest)
  }
}
